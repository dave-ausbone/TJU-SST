TJU-SST
===================

This repository was used for TianJin University's Sensor and Sensoring Technology class.
In it you can find a small POC project that I did for this school subject.

----------

Project
-------------------

This project's goal was to use an Arduino, one or multiple sensors, connect that Arduino to a computer and display data coming from that sensor.

So, what I did was a light switch that reacts to sound so that I could switch on/off an electrical device such as a lamp by clapping in my hands. I also connected this to the cloud so that I could switch on/off the electrical device using my phone or anything that can connect to the web.

#### Material

* Arduino Nano
* Relay module (any 5v relay will work but you'll need other components)
* FC-04 noise sensor
* A couple of female/male cables to connect the components
* AC male plug
* AC female plug
* AC power wire