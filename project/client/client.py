import json
import datetime
from flask import Flask
from flask import jsonify
from flask import render_template
from flaskext.mysql import MySQL

app = Flask(__name__)
mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'toto'
app.config['MYSQL_DATABASE_DB'] = 'sensors'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route('/', methods=['GET'])
def index():
        return render_template('index.html')

@app.route('/logs', methods=['GET'])
@app.route('/logs/<int:last_item>', methods=['GET'])
def get_logs(last_item=None):
        cursor = mysql.connect().cursor()
        if last_item is None:
                cursor.execute("SELECT * from logs")
        else:
                cursor.execute("SELECT * from logs WHERE id > " + str(last_item))
        data = cursor.fetchall()
        if data is None:
                return 'error'
        logs = {}
        for x in data:
                logs.update({x[0]: {"action": str(x[2]), "timestamp": str(x[1])}})
        return jsonify(logs)

@app.route('/order/<string:action>', methods=['POST'])
def send_order(action=None):
        if action == None:
                return jsonify("error"), 400
        connection = mysql.get_db()
        cursor = connection.cursor()
        cursor.execute("INSERT INTO logs(timestamp, action) VALUES(NOW(), \"" + action + "-REQ\")")
        connection.commit()
        return jsonify("ok")

if __name__ == "__main__":
        app.run(host='0.0.0.0')
