const SerialPort = require('serialport');
const MySQLEvents = require('mysql-events');
const mysql = require('mysql');

/* Write to database */
const connection = mysql.createConnection({
    host:	'localhost',
    user:	'root',
    password:	'toto',
    database:	'sensors'
});

/* Arduino connection */
const parsers = SerialPort.parsers;

const parser = new parsers.Readline({
    delimiter: '\r\n'
});

const port = new SerialPort('/dev/ttyUSB0', {
    baudRate: 9600
});

port.on('open', () => {
    console.log('[LOG] Port opened');
    connection.connect();
    connection.query('INSERT INTO logs(timestamp, action) VALUES(NOW(), ?)', ['START'], function (error, results, fields) {});
    connection.query('INSERT INTO logs(timestamp, action) VALUES(NOW(), ?)', ['OFF'], function (error, results, fields) {});
});

port.on('close', () => {
    console.log('[LOG] Port closed');
    connection.end();
});

port.on('data', (data) => {
    let action = data.toString().slice(0, -2);
    connection.query('INSERT INTO logs(timestamp, action) VALUES(NOW(), ?)', [action == 'N' ? 'ON' : 'OFF'], function (error, results, fields) {
    });
});

/* Watch database for orders */
const db = {
    host:	'localhost',
    user:	'root',
    password:	'toto'
};

const mysqlEventWatcher = MySQLEvents(db);

const watcher = mysqlEventWatcher.add(
    'sensors.logs',
    function(oldRow, newRow, event) {
        if (newRow.fields.action === "ON-REQ") {
            port.write("N");
        } else if (newRow.fields.action === "OFF-REQ") {
            port.write("F");
        }
    }
);
