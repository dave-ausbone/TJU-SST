#define RELAY_PIN         7
#define SENSOR_PIN        2
#define SERIAL_BAUD       9600

#define SWITCH_ON  'N'
#define SWITCH_OFF 'F'

int lastSoundValue;
int soundValue;
long lastNoiseTime = 0;
long currentNoiseTime = 0;
long lastLightChange = 0;

int relayStatus = HIGH;
int newRelayStatus;

char serialInput = 0;

void setup() {
  Serial.begin(SERIAL_BAUD);
  pinMode(SENSOR_PIN, INPUT);
  pinMode(RELAY_PIN, OUTPUT);
}

void serialSwitch() {
  if (Serial.available() > 0) {
    serialInput = Serial.read();
    if (serialInput == SWITCH_ON || serialInput == SWITCH_OFF) {
      newRelayStatus = (serialInput == SWITCH_ON) ? HIGH : LOW;
      if (relayStatus != newRelayStatus) {
        relayStatus = newRelayStatus;
        digitalWrite(RELAY_PIN, relayStatus);
        Serial.println(relayStatus == HIGH ? SWITCH_ON : SWITCH_OFF);
      }
      serialInput = 0x00;
   }
  }
}

void sensorSwitch() {
  soundValue = digitalRead(SENSOR_PIN);
  currentNoiseTime = millis();

  if (soundValue == 1) {
    if ((currentNoiseTime > lastNoiseTime + 200) &&
        (lastSoundValue == 0) &&
        (currentNoiseTime < lastNoiseTime + 800) &&
        (currentNoiseTime > lastLightChange + 1000)) {
      relayStatus = !relayStatus;

      digitalWrite(RELAY_PIN, relayStatus);
      lastLightChange = currentNoiseTime;
     }
     lastNoiseTime = currentNoiseTime;
  }
  lastSoundValue = soundValue;  
}

void loop() {
  serialSwitch();
  sensorSwitch();
}

